%% clear and restore path...
clear all global

restoredefaultpath

%% add the toolbox to the path...
addpath('my_toolbox');

%% get a human and let him say hello
my_first_human = example01.Human();
my_first_human.hello()

%% assign and retrieve property...
my_first_human.name = 'Adam';
my_first_human.name

%% create a second human...
my_second_human = example01.Human();
my_second_human.name = 'Eve';

%% now see what the result is:
my_first_human
my_second_human

%% try out the say_name function...
my_first_human.say_name();
my_second_human.say_name();