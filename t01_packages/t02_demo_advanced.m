%% clear and restore path...
clear all global

restoredefaultpath

%% add the toolbox to the path...
addpath('my_advanced_toolbox');

%% now we can call the functions...
mother.anna.my_fun();

mother.max.my_fun();

mother.anna.my_other_fun();